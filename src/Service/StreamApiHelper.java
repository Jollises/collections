package Service;

import Service.TextService.Splitter;
import Service.TextService.TextSplitter;
import Service.WordService.ListEditor;

import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamApiHelper {

    public HashMap<String, Long> getWordCountMap(String textFile) {

        Splitter textSplitter = new TextSplitter();
        String[] words = new String[0];
        try {
            words = textSplitter.splitText(textFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return (HashMap<String, Long>) Arrays.stream(words).collect(Collectors.toMap(w -> w, w -> 1L, Long::sum));

    }

    public List<String> getUniqueWords(HashMap<String, Long> wordMap) {

        return wordMap.keySet().stream().distinct().collect(Collectors.toList());
    }

    public List<String> getSortedStreamByDesc(List<String> stringList) {

        ListEditor listEditor = new ListEditor();
        listEditor.listToLowerCase(stringList);
        return stringList.stream().sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
    }


}
