package Service.TextService;

import Data.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TextSplitter  implements Splitter {

    @Override
    public String[] splitText(String textFile) {

        Reader textReader = new TextReader();

        BufferedReader bufferedReader = null;
        try {
            bufferedReader = textReader.createReader(textFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Text textString = null;
        try {
            textString = getText(bufferedReader);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (textString != null) {
            String[] words = textString.getText().split("[)(?,\\s:!.{3}\n\r\"\\-;]");
            return removeEmptySpace(words);
        } else {
            return new String[0];
        }
    }

    @Override
    public String[] removeEmptySpace(String[] array) {

        List<String> list = new ArrayList<>();

        for(String str : array) {
            if(str != null && str.length() > 0) {
                list.add(str);
            }
        }
        return list.toArray(new String[list.size()]);
    }

    private Text getText(BufferedReader bufferedReader) throws IOException {

        StringBuilder stringBuilder = new StringBuilder("");
        String str;
        if (bufferedReader != null) {
            while ((str = bufferedReader.readLine()) != null) {
                stringBuilder.append(str);
                stringBuilder.append(System.lineSeparator());
            }
        }
        Text text = new Text();
        text.setText(stringBuilder.toString());
        return text;
    }
}
