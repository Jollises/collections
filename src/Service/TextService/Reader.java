package Service.TextService;

import java.io.BufferedReader;
import java.io.IOException;

public interface Reader {

    BufferedReader createReader(String fileName) throws IOException;
}
