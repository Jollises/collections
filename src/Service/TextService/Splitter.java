package Service.TextService;

import java.io.IOException;

public interface Splitter {

    String[] splitText(String text) throws IOException;

    String[] removeEmptySpace(String[] array);
}
