package Service.TextService;

import java.io.*;

public class TextReader implements Reader {

    @Override
    public BufferedReader createReader(String fileName) {
        File file = new File(fileName);

        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return bufferedReader;
    }
}
