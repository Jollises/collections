package Service.WordService;

import java.util.HashMap;

public interface Counter {

    HashMap<String, Long> getWordCountMap(String textFile);
}
