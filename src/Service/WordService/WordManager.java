package Service.WordService;

import Service.TextService.Splitter;
import Service.TextService.TextSplitter;

import java.io.IOException;
import java.util.*;

public class WordManager implements Counter, Unique {

    @Override
    public HashMap<String, Long> getWordCountMap(String textFile) {

        HashMap<String, Long> hashMap = new HashMap<>();
        Splitter textSplitter = new TextSplitter();
        String[] words = new String[0];
        try {
            words = textSplitter.splitText(textFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (String word : words) {
            if (hashMap.containsKey(word)) {
                hashMap.put(word, hashMap.get(word) + 1);
            } else {
                hashMap.put(word, (long) 1);
            }
        }

        return hashMap;
    }

    @Override
    public List<String> getUniqueWords(HashMap<String, Long> wordMap) {

        return new ArrayList<>(wordMap.keySet());
    }

    public void sortByDesc(List<String> wordList) {

        ListEditor listEditor = new ListEditor();
        listEditor.listToLowerCase(wordList);
        wordList.sort(Comparator.reverseOrder());
    }

}
