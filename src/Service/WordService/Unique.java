package Service.WordService;

import java.util.HashMap;
import java.util.List;

public interface Unique {

    List<String> getUniqueWords(HashMap<String, Long> wordMap);
}
