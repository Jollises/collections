package Service.WordService;

import java.util.List;
import java.util.ListIterator;

public class ListEditor {

    public void listToLowerCase(List<String> stringList) {

        ListIterator<String> iterator = stringList.listIterator();
        while (iterator.hasNext())
        {
            iterator.set(iterator.next().toLowerCase());
        }
    }
}
