import Service.StreamApiHelper;
import Service.WordService.WordManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    //text link: https://docs.oracle.com/javase/7/docs/api/java/util/HashMap.html
    public static void main(String[] args) {

        WordManager wordManager = new WordManager();
        HashMap<String, Long> wordMap = wordManager.getWordCountMap("Text.txt");

        for (Map.Entry<String, Long> entry : wordMap.entrySet()) {

            System.out.println("Word: " + entry.getKey() + " | number of repetitions: " + entry.getValue());
        }
        List<String> wordList = wordManager.getUniqueWords(wordMap);
        System.out.println("Unique words:");
        for (String word : wordList) {
            System.out.println(word);
        }

        System.out.println("Sort by descending:");
        wordManager.sortByDesc(wordList);
        for (String word : wordList) {
            System.out.println(word);
        }

        StreamApiHelper streamApiHelper = new StreamApiHelper();
        wordMap = streamApiHelper.getWordCountMap("Text.txt");
        System.out.println("Words and their count with stream: ");
        for (Map.Entry<String, Long> entry : wordMap.entrySet()) {

            System.out.println("Word: " + entry.getKey() + " | number of repetitions: " + entry.getValue());
        }

        wordList = streamApiHelper.getUniqueWords(wordMap);
        System.out.println("Unique words with stream: ");
        System.out.println();
        for (String word : wordList) {
            System.out.println(word);
        }

        System.out.println("Sorted with stream:");
        System.out.println();
        streamApiHelper.getSortedStreamByDesc(wordList).forEach(System.out::println);
    }
}
